var dukeItemStatusMap = new function() {
	 // contains a mapping of the Duke ALEPH loan/item status labels, 
	 // mapped to more Endeca-normative circulation status labels
	this.mappings = {
		"Temporarily Unavailable" : "Not Available",
		"On Shelf" : "Available",
		"LSC" : "Available",
		// RT #262727 -- update LSC such that it reads "Available (Library use Only)"
		// "LSC" : "Available (Library Use Only -  %placerequest%)",
		"Non-circulating" : "Available - Library Use Only",
		"Library use only" : "Available - Library Use Only",
		"Reading Room use only" : "Available - Library Use Only",
		"On Hold" : "On Hold",
		"Requested" : "On Hold",
		"Recently labeled in LC" : "Ask at Circulation Desk",
		"In process-LC" : "Ask at Circulation Desk",
	  "Rubenstein Move Prepped" : "Available",
	  "Rubenstein Move Print Prepped" : "Ask at Circulation Desk",
		"In process-LC Cataloging Dept." : "Ask at Circulation Desk",
		"Sent to Binding" : "Being Repaired",
		"Preservation" : "Being Repaired",
		"Bindery-Boxing" : "Being Repaired",
		"Preservation-Reformatting" : "Being Repaired",
		"Ready to Bind" : "Being Repaired",
		"Commercial Bindery" : "Being Repaired",
		"In process-Binding" : "Being Repaired",
		"Preservation-Disaster Response" : "Being Repaired",
		"In Route" : "In Transit",
		"In Transit/Sublibrary" : "In Transit",
		"Reshelving" : "Reshelving",
		"Missing" : "Lost/Missing",
		"Lost" : "Lost/Missing",
		"Missing" : "Lost/Missing",
		"Lost/Missing" : "Lost/Missing",
		"On Order" : "On Order",
		"Order Initiated" : "On Order",
		"In process-Acquisitions" : "On Order",
		"In process-Cataloging" : "On Order",
		"In process-Collection Dev." : "On Order",
		"In process-MPU" : "On Order",
		"In process-Documents" : "On Order",
		"In process-Outsourced Cat." : "On Order",
		"Sent to Technical Services" : "On Order",
		"In process" : "On Order",
		"In process" : "On Order",
		"Expected" : "On Order",
		"Claimed" : "On Order",
		"Expected" : "On Order",
		"Publication Delayed" : "On Order",
		"Delivery Delayed" : "On Order",
		"Out of Stock" : "On Order",
		"Out of Print" : "On Order"
	},
	
	this.dueDateValue = null,
	this.dueDateRaw = null,
	this.loanStatus = null,
	this.subLibrary = null,
	
	this.hasLoanStatus = function(loanStatus) {
		return loanStatus in this.mappings;
	},
	
	this.processBarcode = function(barcodeData) {
		
		// get the Sub Library so we can test it later (particularly for 'Rubenstein' or 'University Archives')
		this.subLibrary = barcodeData['sub-library'];

		// now get the status of this item from the circStatusNode.  this isn't exactly straightforward, 
		// because of the somewhat inane way ExLibris has overloaded the circ-status call's <due-date> node.  
		// Rather than only populating this node with just a date if the item is charged, 
		// this node can contain one or more combination of due-date, recall, and item process status data, 
		// often (but not always) separated by "##" delimiters.  
		// We need to strip the value down to either an item process status label *or* a valid (numeric) date, and go from there.
		// identify some raw values for potential use later on
		var dueDateRaw = null;

		if ("due-date" in barcodeData) {
			this.dueDateRaw = barcodeData["due-date"];
		}

		if ("loan-status" in barcodeData) {
			this.loanStatus = barcodeData["loan-status"];
		}

		// only test out due-date (and/or item-status) information if we're dealing with non-null values
		if (this.dueDateRaw != null) {
			// perform a switch to get and parse the right kind of data from dueDateRaw
			var recalledDueDatePrefixText = "Effective due date: "; // this precedes an actual due date, needs to be scrubbed
			switch (true) {
				// test for hashed entries (the really overloaded stuff).  Examples of hashed entries: "In Transit/Sublibrary##effective due date: xx/xx/xxxx", "xx/xx/xxxx##Requested", etc.)
				case (this.dueDateRaw.indexOf("##") >= 0):
					// in the case of hashed entries, we only want the pre-hashmark value
					this.dueDateRaw = this.dueDateRaw.substring(0, this.dueDateRaw.indexOf("##"));
					break;
				// test for stylized due dates from recalled entries (typical entry: "Effective due date: xx/xx/xxxx")
				case (this.dueDateRaw.indexOf(recalledDueDatePrefixText) >= 0):
					// we only want the text *after* the effective due date
					this.dueDateRaw = this.dueDateRaw.substr(
						this.dueDateRaw.indexOf(recalledDueDatePrefixText) + recalledDueDatePrefixText.length
						);
					break;
			}
			// now work with the raw due date to figure out whether we're dealing with an actual due date value...
			var dueDateValue = null; // assume we don't have a real, numeric due date
			var dueDateNumberMatchRE = new RegExp("^[0-9/]+$");
			if (this.dueDateRaw.match(dueDateNumberMatchRE) != null) {
				this.dueDateValue = this.dueDateRaw;
			}
		}
	},
	
	this.itemStatusInformation = function() {

		// ... and finally establish whether we're dealing with a status label from either the loan-status field or from the raw "due date" field.  If we're dealing with a status label, then we'll need to map it to an Endeca-approved label (approved by Duke, that is).
		//
		var statusInformation = "Not Available"; // assume we have a problem with the item (since we've matched on it)
		// in the following switch pattern, order matters.  We want to evaluate true due-dates first, then the loan-status matches, and if that fails, then use a due-date status match next.  This avoids issues with getting "On Shelf" due-dates evaluating to "Available" when the loan-status is something like "Non-circulating".
		switch (true) {
			case (this.dueDateValue != null):
				statusInformation = "Checked Out";
				break;
			case ((this.loanStatus != null) && ((this.loanStatus != "LSC") || (this.loanStatus == this.dueDateRaw)) && (this.loanStatus in this.mappings)):
				// the middle condition was added 8/19/2011 when it was discovered that LSC items on hold were showing up as "available", because LSC items always have "LSC" as their loan-status, and this is matching on the dukeItemStatusMappingsObject (which then shows status as available).  To correct for that, we need to only use "LSC" (mapping to "Available") as a loanStatus when "LSC" is also the dueDateValue.  Otherwise, we skip on to dueDateRaw, which will either have things like holds, or if available, due-date will be "LSC", which maps to "Available"....
				statusInformation = this.mappings[this.loanStatus];
				break;
			case ((this.dueDateRaw != null) && (this.dueDateRaw in this.mappings)):
				statusInformation = this.mappings[this.dueDateRaw];
				break;
		}

		// RT Ticket #262727
		// ==================================================================================================
		if (this.subLibrary == 'Rubenstein Library') {
			//debugger;
			if (statusInformation == 'Available' || statusInformation == 'Available - Library Use Only') {
				// change the "Available" to "Available - Library Use Only (Place Request)"
				statusInformation = 'Available (Library Use Only - %placerequest%)';
			}	
		}
		
		return statusInformation;
	},
	
	this.itemStatus = function() {
		
	}
	
	this.resetSettings = function() {	
		this.dueDateValue = null;
		this.dueDateRaw = null;
		this.loanStatus = null;
		this.subLibrary = null;
	}
}
