// declare any global pieces of information
var $subLibraryAlert = false;

document.skinInstitution = "Duke"; // name of the hosting skin-institution (in this case, Duke)
document.dukeCatalogUtilityServiceURLPrefix = "//library.duke.edu/librarycatalog/";  // this is the generic URL prefix pattern for duke-catalog-utilities URL requests (e.g. live-circs, stack-maps, etc.)
document.dukeLocationGuideServiceURLPrefix = "//library.duke.edu/apps/locationguide/find/";  // this is the generic URL prefix pattern for duke-catalog-utilities URL requests (e.g. live-circs, stack-maps, etc.)
/* the time has come to account for different bookplates for different 'honor' programs */
document.digitalBookplateImage = {
    "default" : "//library.duke.edu/static/librarycatalog/images/default-bookplate.jpg",
    "Honoring With Books." : "//library.duke.edu/static/librarycatalog/images/bookplate-honor-memory.jpg",
		'Adopt' : "//library.duke.edu/static/librarycatalog/images/bookplate-adopt-a-book.jpg"
}

document.digitalBookplateImageURL = "//library.duke.edu/static/librarycatalog/images/bookplate-honor-memory.jpg"; // this is the default/generic bookplate image that is used with digital bookplate's in the Duke interface
document.linkResolverLogoGraphicURL = "//library.duke.edu/imgs/SerSols/GetItAtDuke.gif"; // contains the URL for Duke's link-resolver graphic (openURLResolver service)
document.linkResolverCannedText = "get it@Duke"; // contains the stock phrase used as the linkResolver link (typically for things like 360MARC records)

document.cssPrintStylesheetURLs = ['//library.duke.edu/css/common/print.css']; // contains an array of URL(s) for any extra CSS print-only stylesheet(s).  (These can't currently be loaded in the normal way via the SearchTRLN application's skin-configuration settings, because there are no configuration options for print-only <link> elements

/**
 * This new status mapping is used to parse the `loan-status` value.
 * Such values are in the format of "AAAAAAAAA (A)", where "(A)" is one of 
 * O, R, S, A, N, C, L
 */
var dukeLoanStatusMap = {
	'O'	: 'On Order',
	'R' : 'Being Repaired',
	'S' :	'(Suppressed from view)',
	'A'	:	'Available',
	'N'	:	'Not Available',
	'C'	:	'Ask at Circulation Desk',
	'L' : 'Lost/Missing'
};

var reDukeLoanStatusPrecedence = /^(Claimed|On Exhibit)/;
/**
 * The legacy item mapping, used when `due-date` is the key used to determine
 * a barcode item's status
 */
var dukeItemStatusMappingsObject = {
	"On Shelf" : "Available",
	"Non-circulating" : "Available - Library Use Only",
	"Library use only" : "Available - Library Use Only",
	"Reading Room use only" : "Available - Library Use Only",
	"On Hold" : "On Hold",
	"Requested" : "On Hold",
	"Reshelving" : "Reshelving",
	"Lost" : "Lost/Missing",
	"Standard Loan" : "Available"
}; // contains a mapping of the Duke ALEPH loan/item status labels, mapped to more Endeca-normative circulation status labels

// Inject the moment.js library into the page
// added 2/15/2018
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) { return; }
	js = d.createElement(s); js.id = id;
	js.onload = function() {
		// remote script has loaded
	};
	js.src = "//cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'moment-js'));

// define functions first, then initiate with the document.ready command

// generateLocationGuideLinks() identifies all relevant "location" (collection) table cells on the page, and generates a lightbox-link to an externally hosted application that provides dynamically-generated location information for catalog items based on the item's metadata (barcode, library, call-number, etc.).  The tricky part to this function is that it generates a URL that is expected to be handled as an AJAX request, in order to go to the "true" location page on the hosted application (the interim URL that is generated provides the barcode number to the hosted application, and the hosted application returns location-guide information relevant to that barcode back to the client browser, and then the correct lightbox pops up. Most of the logic is handled in the event listener function buildStackMap that is attached to the generated a-links, though.
function generateLocationGuideLinks(attributePrefix) {
	// first thing to do: establish the onClick eventhandler for the location-guide links that we'll produce in this function (we're using live() so that we can generate HTML strings for the links, b/c apparently using the JQuery method of building HTML objects ($('<a>', {attributeObject}).appendTo(xx)) is quite slow compared to parsing strings.  (see <http://marcgrabanski.com/articles/building-html-in-jquery-and-javascript> for exampels of both ways to do things.  Note that we *should* be using delegate() for performance reasons, but delegate() is only supported in JQuery 1.4 and higher, and we're currently using 1.3.2))
	$("td[" + attributePrefix + "barcode] a, td[" + attributePrefix + "barcode=''] a").live("click", buildStackMap);

	// now on to the meat of generating the links.  We expect that the relevant table cells will have four different identifying pieces of information as attributes: barcode, location, callnumber, and library (all attribute names preceded by the attributePrefix).  However, not all of these attributes will have values.  For the purposes of iterating through all of these cells, though, we can just use the requirement that the table cell have a "barcode" attribute (even if the barcode is blank).  Note that we want to use a fitler on the td selections, so that we only select those entries that are members of the skinInstitution (we don't want location guide links for the other institutions, since that would be somewhat nonsensical (at least using the parent instititution's stackmap scheme).)
	$("td[" + attributePrefix + "barcode], td[" + attributePrefix + "barcode='']").filter(function(index) { return ($(this).closest("tr").attr("institution").toLowerCase() == document.skinInstitution.toLowerCase()); }).each( function() {
		// we have two different URL patterns to deal with here: one for non-barcoded/non-itemized locations (typically serials listing on the results-list), and one for everything else (those items that have barcodes). The two types use two different patterns:
			// non-barcoded items (typically serials): {dukeLocationGuideServiceURLPrefix}/find/{library-name|-}__{collection-name|-}__{call-number-information|-}__
			// everything else: {dukeLocationGuideServiceURLPrefix}/find/{barcode|'unknown'}__
		var currentLocationBarcode = $(this).attr(attributePrefix + "barcode");
		var targetURLPattern = document.dukeLocationGuideServiceURLPrefix;
		if ((currentLocationBarcode != null) && (currentLocationBarcode.length > 0)) {
			targetURLPattern += currentLocationBarcode + "__"; // we don't need to encodeURIComponent here, because it's already been encoded on the server-side
		} else {
			// for non-barcoded materials, get the library, collection, and call-number data (using hyphens for blank data), and append this to the URL (this provides as much information as possible about the material to the location-guide application)
			var tableCellNode = this;
			var desiredIndices = {"library" : null, "collection" : null, "callnumber" : null};
			$.each(desiredIndices, function(name, value) {
				var currentDesiredIndex = $(tableCellNode).attr(attributePrefix + name);
				if ((currentDesiredIndex == null) || ($.trim(currentDesiredIndex).length < 1)) {
					desiredIndices[name] = "-";
				} else {
					desiredIndices[name] = currentDesiredIndex;
				}
				targetURLPattern += desiredIndices[name] + "__"; // we don't need to encodeURIComponent here, because it's already been encoded on the server-side
			});
		}
		// we also want to identify the record-title and/or call-number for this item, so we can append it to the <a> element (for use by the location-guide application).  Getting the relevant record title is a little different depending on whether we're in a list or on a full-record view.  (the former means traversing up to the parent table, then down to the span with a class of "recordTitle"; the latter just means finding the span on the page with a class of "recordTitle" (there should be only one).
		var recordTitle = "";
		switch (document.currentPageViewType) {
			case "full-record":
				recordTitle = $("span.recordTitle").text();
				break;
			default:
				// On the results page, record titles are now marked-up as H3 elements
				recordTitle = $("H3.recordTitle", $(this).closest("table")).text();
				break;
		}
		if ((recordTitle == null) || (recordTitle.length < 1)) {
			recordTitle = "<unknown title>";
		}
		// make sure to encode the recordtitle so it's URL safe
		recordTitle = encodeURIComponent(recordTitle)

		var itemCallNumber = $(this).attr(attributePrefix + "callnumber"); // this is already encoded
		if (itemCallNumber == null) {
			itemCallNumber = "";
		}

		// now create the hyperlink for this item (using the existing text as the link text if it exists)
		// var linkText = "<em>Where is this?</em> ";
		// per request from WIGIT (changed on 5/2/2012 for review)
		var linkText = '<em>Click for map</em>';
		var existingText = $.trim($(this).text());
		if (existingText.length > 0) {
			linkText = existingText + '<br />' + linkText;
		}
		var altText = "View location information of these items";
		$(this).html('<a href="' + targetURLPattern + '" title="' + altText + '" recordTitle="' + recordTitle + '" recordCallNo="' + itemCallNumber + '">' + linkText + '</a>');
	});
}

function fancyboxOnCompleteCallback() {
	$('a.closeFancyBox').live('click', function(evt){
		evt.stopImmediatePropagation();
		$.fancybox.close();
		if ($(this).is('#popupCancel')) {
			return evt.preventDefault();
		}
		return true;
	});
}

// buildStackMap() is a companion (event listener) function for generateLocationGuideLinks.  It performs the AJAX call and (re)sets the onclick method for the affected link.
function buildStackMap(e) {
	// adjust the onclick property so that (on click) the link is augmented with a JSON parameter, and we then get the JSON data for this link.
	var thisALink = this;
	var targetURL = thisALink.href;

	// compile the AJAX options and perform the AJAX call (we want full ajax control so we can handle problems)
	var requestMethod = "GET";
	var requestURL = targetURL;
	var requestData = {"format": "jsonp"};
	var requestReturnType = "jsonp";
	var successFunction = function(data, textStatus) {
		// type (map|link), url, error are the attributes
		$(thisALink).unbind(); // reset the a link to blank
		thisALink.href = data.url; // get the returned-data url (the true target)
		// make sure there's a data.type (either map or link)
		if ((data.type != null) && ((data.type == "map") || (data.type == "link"))) {
			// get lightbox title (for fancybox)
			if (data.title) {
				thisALink.title = data.title;
			}
			// adjust the URL to include the record title and call-number grouping (for display purposes) (parameters: title, callno)
			var recordTitle = $(thisALink).attr("recordTitle");
			var recordCallNo = $(thisALink).attr("recordCallNo");
			var extraParameters = "";
			if ((recordTitle != null) || (recordCallNo != null)) {
				extraParameters += "?";
				if (recordTitle != null) {
					extraParameters += "title=" + recordTitle + "&";
				}
				if (recordCallNo != null) {
					extraParameters += "callno=" + recordCallNo;
				}
			}
			thisALink.href += extraParameters;

			// now figure out if we're dealing with a map (data.type "map") or a message with link (data.type "link")
			var lightboxWidth = null;; // this will use the default width (map style)
			var lightboxHeight = null; // this will use the default height (map style)
			if (data.type == "link") {
				lightboxWidth = ".66"; // slightly narrower than map size
				lightboxHeight = ".40"; // somewhat shorter than map size
			}
			var lightboxOptions = calculateWindowBasedLightboxOptions(lightboxHeight, lightboxWidth); // we need to dictate the size of the lightbox, since fancybox can't calculate sizes of external content, and the size of the box varies by the "type" of information (data.type) ...

			$(thisALink).fancybox(lightboxOptions);
		} else {
			// when there's no data.type, just send the user through (this means that there's aproblem with the data that we can't handle here)
			$(thisALink).click(popUpWindow);
		}
		$(thisALink).click();
	};
	var errorFunction = function(XMLHttpRequest, textStatus, errorThrown) {
		return true; // this should force the link to go through without JavaScript
	};
	performAJAXCallGeneric(requestMethod, requestURL, requestData, requestReturnType, successFunction, errorFunction);
	return e.preventDefault();
}

/* The popUpWindow() function is an event listener that provides the pop-up window functionality for external links.  The basic idea is that the function opens a window and then turns the focus onto it.  If you want the <a> link to disply in the href attribute (and to work in older browsers or nonJavaScript-enabled ones), make sure to use the onClick function to call this function and then call "return false;" (that will prevent the href link from working in javascript-enabled browsers). */
function popUpWindow(e) {
	var popUpWin = window.open(this.href, 'externallinkwindow'); // windowName);
	popUpWin.focus();
	return false;
}

// calculateWindowBasedLightboxOptions() is a helper function for generating a list of lightbox options for the fancybox plugin.  It's intended for external-url-generated lightboxes, where fancybox doesn't have any reliable way of knowing how big the content will be.  All that needed for this function is to indicate the relative height/width percentage size of the desired lightbox window (expressed as a percentage of the viewport size) (optional), and (if desired) an indicator not use the overlay option (shading in the background).
function calculateWindowBasedLightboxOptions(windowHeightPercentage, windowWidthPercentage, showOverlayToggle) {
	var windowHeight = $(window).height();
	if (windowHeight == null) {
		windowHeight = 400;
	}
	var windowWidth = $(window).width();
	if (windowWidth == null) {
		windowWidth = 800;
	}
	var desiredHeightPercentage = ".75";
	if (windowHeightPercentage != null) {
		desiredHeightPercentage = windowHeightPercentage;
	}
	var desiredWidthPercentage = ".75";
	if (windowWidthPercentage != null) {
		desiredWidthPercentage = windowWidthPercentage;
	}
	lightboxOptions = {
		'frameWidth' : (desiredWidthPercentage * windowWidth),
		'frameHeight' : (desiredHeightPercentage * windowHeight),
		'zoomSpeedIn' : 100,
		'xoomSpeedOut' : 100
	};
	if (showOverlayToggle !== false) {
		lightboxOptions.overlayShow = true;
		lightboxOptions.overlayOpacity = .5;
	}
	return lightboxOptions;
}


/** 
 * function performLiveCircLookups
 * performLiveCircLookups() is the function that drives the live-circulation-status overrides in the interface.  
 *It is enacted on every page (whether or not there are titles present).
 * The basic process is to look up the title-local-ids on the document, then use external tools to perform 
 * asynchronous lookups using the ALEPH X-Server (which means it must go through a proxy), and 
 * then find any matching barcodes on the document, and replacing existing text with the live status text.
*/
function performLiveCircLookups(spanAttributeNameWithUniqueIdentifiers) {
	// first collect all the record identifiers on the document from which we perform lookups 
	// (this unfortunately includes serials as well as non-serials, but not much we can do about that...).
	// Store these record identifiers as a single delimited string (to be used in a URL)
	var sysnosList = $("span[titlelocalid][institution]").map(function() {
		if ($(this).attr("institution").toLowerCase() == document.skinInstitution.toLowerCase()) {
			return $(this).attr("titlelocalid");
		}
	}).get();
	// only proceed if we're dealing with one or more localIDs...
	if (sysnosList.length > 0) {
		// make sure to identify what type of page we're on -- if this is *not* 
		// a full-record view and/or we have more than one sysnos, then we want to indicate a timeout 
		// in our request for circ statuses.
		// (If we don't specify a timeout for results-list-like views, then we're stuck 
		// waiting on how long it takes to get the title back with the most number of items 
		// associated with it, which can delay considerably the live-data updates on the page for *all* titles.)
		var timeoutValue = 0; // assume no timeout is desired

		if ((sysnosList.length > 1) || (document.currentPageViewType != "full-record")) {
			// 4 seconds is a reasonable amount of time per circ-status request, don't you think? 
			// (the circ-status requests will be performed in parallel for each sysno on the backend, 
			// so this is effectively a timeout indication for both the *total* and *indvidiual* requests 
			// simultaneously -- although only the latter is explicitly going to get set on the backend)	
			timeoutValue = 4; 
		}

		// and now compile the AJAX-based information
		var requestMethod = "GET";
		var requestURL = document.dukeCatalogUtilityServiceURLPrefix + "circstatus/" + sysnosList.join(";") + "/";
		var requestData = {"key" : "barcode"};
		if (timeoutValue > 0) {
			requestData.timeout = timeoutValue;
		}
		var requestReturnType = "jsonp";
		// now that the data has been sent (and received), check to see if any errors.  
		// If so, then we're sort of at the end of the line here 
		// (although it might be nice to write something to the console, but there's no easy cross-browser-compatible way of doing so...).  
		// If not, then  iterate through the results data and update the circulation statuses where applicable.

		var successFunction = function(receivedData, status) {
			// since we seem to have data, let's iterate over the displayed barcode items 
			// on the page, and adjust them if necessary, using the appropriate "property" 
			// data from the receivedData object. (the receivedData object is expected to 
			// be an object with a variety of properties attached to it whose property-names 
			// are barcode values, and whose property-values are objects with two keys: 
			// 		"data" (an object with multiple properties corresponding to the item-data node in an ALEPH circ-status API call), 
			// 		"sys_no" (a string containing the item's parent bib-record system number)


			var now = moment(new Date());

			$("td[itembarcode]").each( function() {
				// identify the barcode value (if non-zero-length), using the *status* (not *location*) table cell data
				var currentBarcode = decodeURIComponent($(this).attr("itembarcode"));
				if (currentBarcode == "1813534-2630")
					debugger;

				if ((currentBarcode.length > 0) && (currentBarcode in receivedData) && ("data" in receivedData[currentBarcode])) {
					var currentCircStatusNode = receivedData[currentBarcode]["data"];

					// get the Sub Library so we can test it later (particularly for 'Rubenstein' or 'University Archives')
					var subLibrary = currentCircStatusNode['sub-library'];

					// now get the status of this item from the circStatusNode.  
					// this isn't exactly straightforward, because of the somewhat inane way 
					// ExLibris has overloaded the circ-status call's <due-date> node.  
					// Rather than only populating this node with just a date if the item is charged, 
					// this node can contain one or more combination of due-date, recall, and item process status data, 
					// often (but not always) separated by "##" delimiters.  
					// We need to strip the value down to either an item process status label *or* a valid (numeric) date, and go from there.
					// identify some raw values for potential use later on
					var dueDateRaw = null;
					var dueDateValue = null; // assume we don't have a real, numeric due date
					var statusInformation;
					var useNewStatusDefinitions = false;
					
					// used when the ## pattern is present in dueDateValue
					var dueDateParts = [];

					var loanStatus = null;
					var loanIndicator = false;
					var loanStatusInformation = false;
					var itemDateInfo = {
						isDate : false, 
						dueDateValue : '',
						dueDateStatus : '',
						loanStatus : '',
						validDueDate : false,
						finalItemStatus : false,
						nonCirculating : false,
						singleLetterLoanStatus : false,
						itemIsInExhibit : false
					};

					/* 
						when determining if 'loan-status' is available for 
						a particular barcode, then attempt to parse this convention:

						Loan Status (x)
						where 'x' is one of: (O|R|S|A|N|C|L)

						When this convention is detected, store the loan-status information
						in case we need it.
					*/
					if ("loan-status" in currentCircStatusNode) {
						loanStatus = currentCircStatusNode["loan-status"];
						itemDateInfo.loanStatus = loanStatus;

						/**
						 * This block accounts for the XXXXXX (Y) convention, 
						 * where "XXXXXX" is some text status and Y is some one-letter coded
						 * (see array at top of this file for array definition)
						 */

						// changed on 4/18/2018 to include group match of the text preceeding (Y)
						loanStatusRegExp = /^(.+)\((O|R|S|A|N|C|L)\)$/;
						var lmatches = loanStatus.match( loanStatusRegExp );
						if (lmatches != null) {
							// use the status notated by the single letter in the parentheses
							// ALEPH change implemented by Karen Newbery/Jeff Fleming
							// put into production on 5/24/2017

							// fetch the loan status indicator from the match array
							itemDateInfo.loanStatusText = lmatches[1].trim();
							loanIndicator = lmatches[2];
							itemDateInfo.loanStatus = dukeLoanStatusMap[loanIndicator];
							itemDateInfo.singleLetterLoanStatus = true;

							// account for certain loan status verbage
							// for example (look for "Claimed")
							// TASK2738754 (4/18/2018)
							itemDateInfo.loanStatusHasPrecedence = (itemDateInfo.loanStatusText.match( reDukeLoanStatusPrecedence ) != null)

							// Exhibit item?
							// TASK2518756 (added 4/18/2018)
							itemDateInfo.itemIsInExhibit = itemDateInfo.itemIsInExhibit || itemDateInfo.loanStatusText == 'On Exhibit';
						}
						itemDateInfo.nonCirculating = (itemDateInfo.loanStatus == "Non-circulating");
					}

					if ("due-date" in currentCircStatusNode && currentCircStatusNode["due-date"] != null) {
						dueDateRaw = currentCircStatusNode["due-date"];

						// ... and finally establish whether we're dealing with a status label 
						// from either the loan-status field or from the raw "due date" field.  
						// If we're dealing with a status label, then we'll need to map it to an 
						// Endeca-approved label (approved by Duke, that is).
						
						// assume we have a problem with the item (since we've matched on it)
						statusInformation = "Not Available"; 

						// now work with the raw due date to figure out whether we're dealing with an actual due date value...
						var dueDateNumberMatchRE = new RegExp("[0-9/]+");
						//var dueDateNumberMatchRE2 = new RegExp("^([\w\s]+)?([0-9/]+)([.]+)?$");

						// create a 'catch-all' RE pattern that looks for the following:
						// 1) MM/DD/YY due date optionally preceeded (and proceeded) by text status
						// 2) Due date "text status" optionally proceeded by MM/DD/YY date pattern
						// 3) Text or Date ## Text or Date
						// Array parts:
						// [1] for #1 above
						// [2-4] captures for [0]
						// [5] for #2 above
						// [6-8] catures for [4]
						// [9] #3 above
						// [12] is for "Effective due date: MM/DD/YY"
						// [16] LSC (single character status)
						//var dueDateStatusLookupRE = new RegExp("^(([a-zA-z\\s]+)?([0-9/]+)([a-zA-Z\\s]+)?)?(([a-zA-z\\s]+)?([0-9/]+)?([a-zA-Z\\s]+)?)?((([a-zA-z\\s]{1,})|([\\d]{2}\/[\\d]{2}\/[\\d]{2,4}))(?:#{2})(([a-zA-z\\s]{1,})|([\\d]{2}\/[\\d]{2}\/[\\d]{2,4})))?(Effective Due Date\: ([0-9/]+))?(LSC.*)?$");
						//var allMatches = dueDateRaw.match(dueDateStatusLookupRE);

						// the "catch-all" pattern has changed:
						// Case 1: "Effectvie Due Date MM/DD/YY"
						// Case 2: "LSC"
						// Case 3: The double "##" case
						// Case 4: MM/DD/YY due date optionally proceeded or preceeded by text status
						// Case 5: Text Status optionally proceeded by MM/DD/YY
						var dueDateStatusLookupRE = new RegExp("^(Effective Due Date\: ([0-9/]+))?(LSC.*)?((([a-zA-z\\s]{1,})|(?:[\\d]{2}\/[\\d]{2}\/[\\d]{2,4}))(?:#{2})(([a-zA-z\\s]{1,})|(?:[\\d]{2}\/[\\d]{2}\/[\\d]{2,4})))?(([a-zA-z\\s]+)?([0-9/]+)([a-zA-Z\\s]+)?)?(([a-zA-z\\s]+)?([0-9/]+)?([a-zA-Z\\s]+)?)?$");
						var allMatches = dueDateRaw.match(dueDateStatusLookupRE);						
						if (allMatches == null) {
							// the only known case that is not accounted for 
							// in the above regexp is "some status (X)"
							// in this case, loan-status eq due-date, and we've already
							// parsed the loan-status in the above IF block

							// I don't think we need to do anything here because
							// loan-status will be folded up into "finalItemStatus" further down 
							// in the processing...

						} else {
							// Effective Due Date
							if (allMatches[1] !== undefined) {
								itemDateInfo.dueDateValue = allMatches[2];
								itemDateInfo.isDate = true;
								itemDateInfo.validDueDate = true;
								//itemDateInfo.validDueDate = now.isSameOrBefore( moment(itemDateInfo.dueDateValue, "MM/DD/YY") );
							}
							// LSC
							if (allMatches[3] !== undefined) {
								itemDateInfo.finalItemStatus = itemDateInfo.loanStatus;
							}
							// Double ## pattern
							if (allMatches[4] !== undefined) {
								itemDateInfo.dueDateStatus = (allMatches[6] !== undefined) ? allMatches[6] : allMatches[7];
								itemDateInfo.dueDateValue = (allMatches[6] === undefined) ? allMatches[5] : allMatches[7];
								itemDateInfo.validDueDate = true;
								//itemDateInfo.validDueDate = now.isSameOrBefore( moment(itemDateInfo.dueDateValue, "MM/DD/YY") );
							}
							// Due date MM/DD/YY preceeded or proceeded by text
							if (allMatches[9] !== undefined) {
								itemDateInfo.dueDateValue = allMatches[11];
								itemDateInfo.isDate = true;
								itemDateInfo.validDueDate = true;
								//itemDateInfo.validDueDate = now.isSameOrBefore( moment(itemDateInfo.dueDateValue, "MM/DD/YY") );
							}
							// text status 
							if (allMatches[13] !== undefined) {
								itemDateInfo.dueDateStatus = allMatches[14].trim();
							}
						}


						// dueDateRaw contains MM/DD/YY
						if (dueDateRaw.match(dueDateNumberMatchRE) != null) {
							itemDateInfo.isDate = true;
							dueDateValue = dueDateRaw;
							// Due Date has precedent over newer status implementation
							// only test out due-date (and/or item-status) information if we're dealing with non-null values
							if (dueDateRaw != null) {
								// perform a switch to get and parse the right kind of data from dueDateRaw
								var recalledDueDatePrefixText = "Effective due date: "; // this precedes an actual due date, needs to be scrubbed
								switch (true) {
									// test for hashed entries (the really overloaded stuff).  Examples of hashed entries: "In Transit/Sublibrary##effective due date: xx/xx/xxxx", "xx/xx/xxxx##Requested", etc.)
									case (dueDateRaw.indexOf("##") >= 0):
										// in the case of hashed entries, we only want the pre-hashmark value
										// TASK2584633
										dueDateParts = dueDateRaw.split("##");
										if (isNaN(dueDateRaw.substring(0, 1))) {
											dueDateRaw = dueDateRaw.substring(0, dueDateRaw.indexOf("##"));
											dueDateParts.reverse();
										} else {
											dueDateRaw = dueDateRaw.substring(dueDateRaw.indexOf("##") + 2);
										}
										break;
									// test for stylized due dates from recalled entries (typical entry: "Effective due date: xx/xx/xxxx")
									case (dueDateRaw.indexOf(recalledDueDatePrefixText) >= 0):
										// we only want the text *after* the effective due date
										// provides a date in MM/DD/YY format
										dueDateRaw = dueDateRaw.substr(dueDateRaw.indexOf(recalledDueDatePrefixText) + recalledDueDatePrefixText.length);
										break;
								}
							}
							// in the following switch pattern, order matters.  We want to evaluate true due-dates first, then the loan-status matches, and if that fails, then use a due-date status match next.  This avoids issues with getting "On Shelf" due-dates evaluating to "Available" when the loan-status is something like "Non-circulating".
							switch (true) {
								case (dueDateValue != null):
									statusInformation = "Checked Out";
									break;
								case ((loanStatus != null) && ((loanStatus != "LSC") || (loanStatus == dueDateRaw)) && (loanStatus in dukeItemStatusMappingsObject)):
									// the middle condition was added 8/19/2011 when it was discovered that LSC items on hold were showing up as "available", because LSC items always have "LSC" as their loan-status, and this is matching on the dukeItemStatusMappingsObject (which then shows status as available).  To correct for that, we need to only use "LSC" (mapping to "Available") as a loanStatus when "LSC" is also the dueDateValue.  Otherwise, we skip on to dueDateRaw, which will either have things like holds, or if available, due-date will be "LSC", which maps to "Available"....
									statusInformation = dukeItemStatusMappingsObject[loanStatus];
									break;
								case ((dueDateRaw != null) && (dueDateRaw in dukeItemStatusMappingsObject)):
									statusInformation = dukeItemStatusMappingsObject[dueDateRaw];
									break;
							}
						} else if (dueDateRaw.indexOf("##") >= 0) {
							var res = dueDateRaw.split("##");
							var isOnHold = false;
							for(var i = 0; i < res.length; i++) {
								if (res[i] == "On Hold") {
									isOnHold = true;
									break;
								}
							}
							statusInformation = isOnHold ? "On Hold" : dukeItemStatusMappingsObject[loanStatus];
						} else {
							if (dukeItemStatusMappingsObject[loanStatus]) {
								statusInformation = dukeItemStatusMappingsObject[loanStatus]
							} else if (dueDateRaw == "On Shelf") {
								statusInformation = "Available";
							} else {
								useNewStatusDefinitions = true;
							}
						}
					}

					// NEW LOGIC
					if (!itemDateInfo.finalItemStatus) {
						if (itemDateInfo.isDate && !itemDateInfo.loanStatusHasPrecedence) {
							itemDateInfo.finalItemStatus = itemDateInfo.validDueDate 
								? "Checked Out" 
								: (dukeItemStatusMappingsObject[itemDateInfo.dueDateStatus] !== undefined)
									? dukeItemStatusMappingsObject[itemDateInfo.dueDateStatus]
									: itemDateInfo.loanStatus; 
						} else if (itemDateInfo.itemIsInExhibit) {

							// In response to TASK2518756
							itemDateInfo.finalItemStatus = 'On Exhibit';
						} else {
							// use the new Loan Status pattern in the abs
							itemDateInfo.finalItemStatus = (dukeItemStatusMappingsObject[itemDateInfo.dueDateStatus] !== undefined)
								? dukeItemStatusMappingsObject[itemDateInfo.dueDateStatus]
								: itemDateInfo.loanStatus;
						}
					}
					statusInformation = itemDateInfo.nonCirculating ? dukeItemStatusMappingsObject["Non-circulating"] : itemDateInfo.finalItemStatus;

					// PLEASE keep this debugger statement in place
					// debugger;

					// RT Ticket #262727
					// ==================================================================================================
					if (subLibrary == 'Rubenstein Library' || subLibrary == 'University Archives') {
						if ($subLibraryAlert != false) {
							$('.subLibLabel').hide();
							$('.subLibLabel[subLib="' + subLibrary + '"]').show();
							$subLibraryAlert.show();
						}

						if (statusInformation == 'Available' || statusInformation == 'Available - Library Use Only') {
							// change the "Available" to "Available - Library Use Only (Place Request)"
							statusInformation = 'Available (Library Use Only - %placerequest%)';
						}	
					}
					if (subLibrary == 'Duke Kunshan Library') {
						if (statusInformation == 'Available') {
							statusInformation = statusInformation + ' (Kunshan campus only)';
						}
					}

					// replace %placerequest% with actual <a> element
					statusInformation = statusInformation.replace('%placerequest%', 
						'<a href="//library.duke.edu/librarycatalog/request/' + receivedData[currentBarcode]['sys_no'] + '">Place Request</a>');
					// ==================================================================================================
					// END of RT #262727


					// now finally we can get to determining what to display to the client, including what class to assign to the containing span element
					var spanClass = "resultAvailability";
					var availableTestRE = new RegExp("^Available.*$");
					if (statusInformation.match(availableTestRE) != null) {
						spanClass += "Available";
					} else {
						spanClass += "CheckedOut";
					}
					// put it all together
					// this section takes into account a new due-date format
					// TASK2584633
					if (itemDateInfo.validDueDate) {
						statusInformation += " (Due " + itemDateInfo.dueDateValue + ")";
					}

					// UNNEEDED, but here for "logic" reference
					//if (dueDateValue != null) {
					//	if (dueDateParts.length > 0) {
					//		dueDateValue = dueDateParts[0];
					//	}
					//	statusInformation += "  (Due " + dueDateValue + ")";
					//}

					$(this).html('<span class="' + spanClass + '">' + statusInformation + '.</span>');
				}
			});
		};
		var errorFunction = function(xhpObject, status, error) {
			document.liveCircStatusIndicator = "failure! status: " + status + ", error: " + error + ", XMLHTTPREQUEST: " + xhpObject.getAllResponseHeaders() + ", responseXML: " + xhpObject.responseXML;
			return false;
		};
		// process the ajax function
		performAJAXCallGeneric(requestMethod, requestURL, requestData, requestReturnType, successFunction, errorFunction);
	}
}

// generateMissingBookAnchorElement() creates a "Request Missing Book" anchor 
// element in the "Export" section (Print, SMS/TEXT, etc) in the Endeca 
// Full Record View
function generateMissingBookAnchorElement(bookTitle, bookAuthor, callNumber) {
	params = []
	params.push('AUTHOR=' + encodeURIComponent(bookAuthor));
	params.push('BTITLE=' + encodeURIComponent(bookTitle));
	if (callNumber.length > 0) {
		params.push('CALLNUM=' + encodeURIComponent(callNumber));
	}
	
	anc = '<a href="https://duke.qualtrics.com/SE/?SID=SV_71J91hwAk1B5YkR' 
		+ '&' 
		+ params.join('&') 
		+ '" class="requestMissing" target="_blank" tracking="Catalog" id="RequestMissing">Request Missing Book</a>';

	$missingBookRow = $(
		'<tr><td colspan="2"><div class="requestMissing"><strong>Not on the shelves?</strong><br />'
		+ anc 
		+ '</div></td></tr>'
		);
	$('TBODY', '#exportPane').append($missingBookRow);
	// now, attach a click event handler
	$('#RequestMissing').click(function() {
		var $tracking = $(this).attr('tracking');
		try {
			_gaq.push(['_trackEvent', 'RequestMissing', 'Export', $tracking]);
		} catch(err) {
			if (console != undefined) {
				console.log(err)
			}
		}
	});
}

// generateBookplateLightboxLinks() is a somewhat complicated function that links donor-bookplate text (typically only on the Details tab of a full-record view) to a fancybox pop-up of a digital bookplate.  What makes this somewhat complicated is that the digital bookplate is generated on the fly as part of the lightbox-link event registration process.
function generateBookplateLightboxLinks(detailsPropertyAttributeValue) {
	// detailsproperty="Donor"
	if (detailsPropertyAttributeValue != null) {
		// work on each details-tab property values that meet the attributeValue criteria
		$("li[detailsproperty=" + encodeURIComponent(detailsPropertyAttributeValue) + "]").each( function() {
			// identify some base values
			var bookplateText = $(this).text();
			var detailsPropertyAttribute = $(this).attr("detailsproperty");
			if (detailsPropertyAttribute != null) {
				detailsPropertyAttribute = decodeURIComponent(detailsPropertyAttribute);
			}
			var donorProgramName = $(this).attr("donorprogram");
			if (donorProgramName != null) {
				donorProgramName = decodeURIComponent(donorProgramName);
			}
			var bookplateImageUrl = document.digitalBookplateImage['default'];
			if (document.digitalBookplateImage[donorProgramName] != undefined) {
			    // use the specific bookplate (e.g. "Honoring With Books")
			    bookplateImageUrl = document.digitalBookplateImage[donorProgramName];
			}
			// we'll first want to isolate the value of the donor property (the bookplate text), minus any possible preceding labels and/or appending donor-program names (format with both aspects will be: "[detailsPropertyAttributeValue]: [bookplate-value] ([donor-program name])")
			var propertyPrefixRegExp = new RegExp("^(" + detailsPropertyAttribute + ":\\s+)(.+)\\s*$");
			var bookplateProgramNameRegExp = new RegExp("(.+)(\\s+\\(" + donorProgramName + "\\)\\s*)$");
			bookplateText = bookplateText.replace(propertyPrefixRegExp, "$2");
			bookplateText = bookplateText.replace(bookplateProgramNameRegExp, "$1");
			// now that the right text is isolated, the bookplate can be constructed.
			// create a randomized number for the bookplate id (just in case there are multiple bookplates for this title)
			var bookplateID = Math.floor(Math.random()*1001); // number between 1 and 1000
			// document.digitalBookplateImageURL
			// now append the bookplate markup
			var bookplateMarkup = $('<div style="display: none"><div id="bookplate' + bookplateID + '"><div class="lightbox"><div class="bookplatecontainer"><div class="bookplateimagecontainer"><img src="' + bookplateImageUrl + '" alt="bookplate image" /></div><ul class="bookplatetext">	<li>' + bookplateText + '</li></ul></div></div></div></div>');
			$("body").append(bookplateMarkup);
			// get height/width of bookplate
			var bookplateHeight = $(".bookplatecontainer").height();
			var bookplateWidth = $(".bookplatecontainer").width();
			// set up the desired fancybox optons for the bookplate
			var bookplateFancyboxOptions = {
				'frameWidth'     : bookplateWidth,
				'frameHeight'    : bookplateHeight,
				'overlayShow'    : 'true',
				'overlayOpacity' : .5
			};
			// and now rewrite the contents of the <li> with a fancybox-linked <a> element with the bookplateText
			var bookplateLink = $('<a href="#bookplate' + bookplateID + '">' + bookplateText + '</a>');
			$(this).html('').append(bookplateLink);
			try {
				bookplateLink.fancybox(bookplateFancyboxOptions);
			} catch( err ) {
				console.log( err );
			}
		});
	}
}

// performAJAXCallGeneric() is a generic-style function that performs an AJAX call according to the requested parameters. (A copy of this function also lives in the SearchTRLN-based application's "export.js" file, but this file is not always loaded on every pagein the application.   Nothing is returned, as the desired actions are executed via the success/failure components.  the $.ajax JQuery method is preferred rather than the simpler .get, .post or .load, as we want to provide some level of error-handling.
function performAJAXCallGeneric(requestMethod, requestURL, requestData, requestReturnType, successFunction, errorFunction, requestReturnContentType) {
	if ((requestMethod != null) && (requestURL != null) && (requestData != null) && (requestReturnType != null) && (successFunction != null) && (errorFunction != null)) {
		// compile ajax options
		var ajaxOptions = { type:requestMethod, url:requestURL, data:requestData, dataType:requestReturnType, success:successFunction, error:errorFunction};
		if (requestReturnContentType != null) {
			ajaxOptions.contentType = requestReturnContentType
		}
		$.ajax(ajaxOptions);
	}
}

// identifyQueryParameters() is a helper function that generates a JavaScript object that contains all of the query parameters and their values, stored as object-like properties and values.  It's used primarily for helping to identify whether we're currently on the full-record view page (where the URL contains an "id" parameter).  This function is based on a user submission to StackOverflow at <http://stackoverflow.com/questions/901115/get-querystring-values-with-jquery/2880929#2880929>.
function identifyQueryParameters() {
	var urlParams = {};
	(function () {
		var e,
			a = /\+/g,  // Regex for replacing addition symbol with a space
			r = /([^&;=]+)=?([^&;]*)/g,
			d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
			q = window.location.search.substring(1);

		while (e = r.exec(q))
		   urlParams[d(e[1])] = d(e[2]);
	})();
	return urlParams;
}

// identifyCurrentPageType() is a relatively simple function that tries to 
// determine what type of page we're looking at (full-record, results-list, etc.).  
// It's a bit of a hack, but necessary in order to perform various skin functions
function identifyCurrentPageType() {
	// get the current URL data, and parse out both the page name and any query parameters
	var currentPageURLPathArray = window.location.pathname.split('/'); // get the path portion of the URL
	document.currentPageViewType = currentPageURLPathArray[currentPageURLPathArray.length - 1]; // this gets us the servlet script name (e.g. "search", "markeditems", etc.)
	// we want to override this pageName with "full-record" if we're on the full-record page, 
	// which currently seems to be the case if the pageName is "search" and there's an "id" 
	// query parameter in the query portion of the URL
	var queryParamsObject = identifyQueryParameters();
	if ((document.currentPageViewType.toLowerCase() == "search") && ("id" in queryParamsObject)) {
		document.currentPageViewType = "full-record";
	}
}

// generateLinkResolverLogoLinks() is a fairly simple function that finds all 
// links that meet a certain criteria in a specified container element, and 
// adjusts those links so that they display the "official" linkResolver logo 
// graphic as the link, rather than text links.  The second argument to this 
// function (skinName) indicates an optional argument that, if the skinName is 
// found on the <body> of the document, applies the linkResolverLogo transformation 
// for *all* links (that are related to the skinInstitution) in the 
// containerClassName (this is helpful for, say the Duke db-finder skin)
function generateLinkResolverLogoLinks(containerClassName, skinName, adjustTitleLinkIndicator) {
	if ((containerClassName != null) && (document.linkResolverCannedText != null) && (document.linkResolverLogoGraphicURL != null)) {
		// determine whether we're dealing with a skinName match (if so, then all links will get transformed)
		var skinNameMatch = false;
		if ((skinName != null) && ($("body").attr("applicationskinname") == skinName)) {
			skinNameMatch = true;
		}
		// iterate through all containers on the page that have the specified class name 
		// (only tds and divs are considered valid container), and alter the links in that 
		// container that have the linkResolver text as their link-text.
		$("td." + containerClassName + " a, div." + containerClassName + " a").each(function() {
			if ((skinNameMatch && (this.href.toLowerCase().indexOf(document.skinInstitution.toLowerCase()) >= 0)) || ($(this).text() == document.linkResolverCannedText) || (skinNameMatch && ($(this).text().indexOf("Open Access resource") >= 0))) {
				$(this).attr('title', document.linkResolverCannedText).html('<img src="' + document.linkResolverLogoGraphicURL + '" alt="' + document.linkResolverCannedText + '" />');
				// there's an additional step here as well: 
				// we want to adjust the title-link for this record so that it uses the current <a> link, 
				// with a "(More details)" link next to it that takes you to the full-record.  
				// this is based on usability tests that indicated that the GetIt@Duke icon link alone 
				// wasn't working for the db-finder, and we need to make the title of the record in the 
				// results-list link to the primary-url.
				if (skinNameMatch && (adjustTitleLinkIndicator != null) && (adjustTitleLinkIndicator === true)) {
					// get the primary URL
					var primaryURL = this.href;

					// get the recordid (as identified by an attribute on the container element with the "primaryurls" class)
					var recordid = $(this).parent().attr("recordid");

					// only proceed if there's a record-id
					if (recordid != null) {

						// iterate through the document's possible title-links for that recordid (should be only one)
						$('span[class="recordTitle"][recordid="' + recordid + '"]').each(function() {

							// first check to see if this span has already been adjusted
							if ($(this).attr("titleLinkModified") != "yes") {
								// get original text and link of <a> title-link inside the span
								var linkTextOriginal = $("a", this).text();
								var linkHrefOriginal = $("a", this).attr("href");

								// now modify the <a> link
								$("a", this).attr("href", primaryURL).attr("target", "_blank");

								// if IE8 or lower and the original link text has an "@" in it, then also reset 
								// the link text to its original (this is because IE8 and lower has a weird bug 
								// involving href changes on links that have the "@" symbol in the link text...)
								if ((linkTextOriginal.indexOf('@') >= 0) && ($.browser.msie && (parseInt($.browser.version) < 9))) {
									$("a", this).text(linkTextOriginal);
								}

								// ... and then append a "more details" link with the original link
								$(this).append(' &nbsp; &nbsp; <span style="font-size: .9em; font-weight: normal; font-style: italic"><a href="' + linkHrefOriginal + '">(More details...)</a></span>');

								// and indicate that the modification has taken place 
								// (so we don't do this multiple times for a title with multiple matching-condition primary-urls.
								$(this).attr("titleLinkModified", "yes");
							}
						});
					}
				}
			}
		});
	}
}

function correctRadioButtonLabels() {
	$(':radio#duke')
		.parent()
			.text('Duke only');

	$(':radio#worldcat')
		.parent()
			.text('WorldCat (no login required)');
}

function processScanThisButton() {
	var bibsToSuppress = ['001447347', '002715221', '003593226', '001425235', '001690702', '002148636', '000542155'];
	var bibMatch = window.location.href.match(/DUKE.*/);
	if (bibMatch != null) {
		if (bibMatch.length == 1) {
			currentBib = bibMatch[0].substr(4);
			if (bibsToSuppress.indexOf(currentBib) != -1) {
				$('#scanthis-button').hide();
			}
		}
	}
}

var orig_ProcessGBSBookInfo = window.ProcessGBSBookInfo;
window.ProcessGBSBookInfo = function(booksInfo) {
	for (identifier in booksInfo) {
		var bookInfo = booksInfo[identifier];
		identifier = identifier.replace(/:/g,"\\:");
		if (bookInfo) {
			if (bookInfo.preview == "full" || bookInfo.preview == "partial"){
				var container = $(".gbsContainer." + identifier); 
				container.css("display", "");
				container.attr("href", bookInfo.preview_url);
				// 4/11/14 changed "Full view available via Google Book Search" to "Full text - Google Books"
				if (bookInfo.preview == "full")					
					container.html("<td width=\"7%\">&nbsp;</td><td width=\"18%\" class=\"lightText\" valign=\"top\">Online Access: </td>" 
					+ "<td width=\"75%\" colspan=\"3\" align=\"left\"><a id=\"googleFullPreview\" href=\"" + bookInfo.preview_url
			 		+ "\" target=\"_blank\">Limited preview - Google Books</a></td>");
			 	
				if (bookInfo.preview == "partial")					
					container.html("<td width=\"7%\">&nbsp;</td><td width=\"18%\" class=\"lightText\" valign=\"top\">Online Access: </td>" 
					+ "<td width=\"75%\" colspan=\"3\" align=\"left\"><a id=\"googlePartialPreview\" href=\"" + bookInfo.preview_url
			 		+ "\" target=\"_blank\">Limited preview - Google Books</a></td>");
				container.removeClass().addClass("gbsContainer");
				return;
			}
		}
	}
}

var orig_ProcessListGBSBookInfo = window.ProcessListGBSBookInfo;
window.ProcessListGBSBookInfo = function(booksInfo) {
	for (identifier in booksInfo) {
		var bookInfo = booksInfo[identifier];
		identifier = identifier.replace(/:/g,"\\:");
		if (bookInfo){
			$(".gbsContainer." + identifier).each(function(){
				if (bookInfo.preview == "full"){
					$(this).css("display", "");
					
					// 4/11/14 - Changed "Full view available via Google Book Search" to "Full text - Google Books"
					$(this).append("<td width=\"7%\">&nbsp;</td><td colspan=\"3\" class=\"brieflibrary\"><a id=\"googleFullPreview\" href=\"" + bookInfo.preview_url
							 + "\" target=\"_blank\">Limited preview - Google Books</a></td><td class=\"briefstatus\">"
							 + "<span class=\"resultAvailabilityOnline\">Online Access</span></td>");
				}
				
				// 4/11/14 changed "Limited preview available via Google Book Search" to "Limited preview - Google Books"
				if (bookInfo.preview == "partial"){
					$(this).css("display", "");
					$(this).append("<td width=\"7%\">&nbsp;</td><td width=\"65%\" colspan=\"3\" class=\"brieflibrary\"><a id=\"googlePartialPreview\"href=\"" + bookInfo.preview_url
							 + "\" target=\"_blank\">Limited preview - Google Books</a></td><td class=\"briefstatus\">"
							 + "<span class=\"resultAvailabilityOnline\">Limited Preview - Google Books</span></td>");
				} 
			}).removeClass().addClass("gbsContainer");
		}
	}
}


/**
 * initialize click events for .recordTitle A elements
 * but only when the <BODY>'s applicationskinname = 'duke-db'
 */
function initDBRecordTitleLinks() {
	var $body = $('[applicationskinname="duke-db"]');
	var $recordTitleAnchors = $('#resultsColumn .itemRecordDetails .recordTitle a', $body);
	var $itemRecord = $('.itemRecord', $body);
	
	// locate all recordTtle elements within the #resultsColumn element
	$('#resultsColumn .itemRecordDetails .recordTitle a', $body).each(function(i, el) {
		var $parent = $(this).parents('.itemRecordDetails');
		var $parentItemRecord = $(this).parents('.itemRecord');
		var fullRecordURL = $(this).attr('href');		
		var $anchor = $('.primaryurls a', $parent);
		var href = $anchor.attr('href');

		// add a 'target' attribute and set it to _blank
		$(this).attr('target', "_blank");

		// instead of changing the element's HREF attribute,
		// set the 'primaryurls' href as window.location.href
		// and allow the browser to handle the rest.
		$(this).click(function(evt) {
			window.location.href = href;
			return evt.preventDefault();
		});
	
		// additionally, as a convience, provide a way for 
		// patrons to access the full record
		$fullRecordAnchor = $('<a href="' + fullRecordURL + '">View Full Record</a>')
		$('.thumbnail-etc', $parentItemRecord).append( $fullRecordAnchor );
	});

}

// use Jquery's document.ready to bundle the actions together (initialize them when the document is truly ready)
$(document).ready(function() {
	// provided a list of Bib numbers from Judy Bailey, hide
	// the Scan This (Digitize This Book) button as needed
	processScanThisButton();
	
	// add any print-only CSS stylesheets
	if (document.cssPrintStylesheetURLs && (document.cssPrintStylesheetURLs instanceof Array) && (document.cssPrintStylesheetURLs.length > 0)) {
		$.each(document.cssPrintStylesheetURLs, function(index, value) {
			$('head').append('<link href="' + value + '" media="print" rel="stylesheet" type="text/css" />');
		});
	}

	// fix for RT ticket #227991
	$('img[title="Get help from a Reference Librarian through our Chat Reference Service"]')
		.attr('title', 'Get help from a Librarian through our Chat Service');

	identifyCurrentPageType();
	try {
		$(document).pngFix();
	} catch(err) {
		if (console != undefined) {
			console.log(err);
		}
	}

	performLiveCircLookups('titlelocalid');

	// prior to doing the LiveCirculation lookups
	// create a blank element that sits just above the "Request" button
	// This element will be used to create a temporary alert message from any sub-library (as-needed)
	if ( $('body').hasClass('fullRecord') ) {
		$subLibraryAlert = $( '<div class="subLibAlert" style="text-align: center !important;"></div>' ).hide();

		// TEMPORARY ARRANGEMENT
		// add all known messages here
		$subLibraryAlert.html(
			'The Rubenstein Library will be closed July 1 &ndash; August 23, 2015. ' +
			'Material held by the <span class="subLibLabel" subLib="Rubenstein Library">Rubenstein Library</span>' + 
			'<span class="subLibLabel" subLib="University Archives">University Archives</span> ' +
			'will be unavailable during this time.&nbsp;&nbsp;' + 
			'<br /><a href="//blogs.library.duke.edu/renovation/for-researchers-and-visitors/" target="_blank">Learn More &raquo;</a>'
		);
		// $( 'TABLE.mainContainer' ).before( $subLibraryAlert );
		$( 'TABLE.mainContainer' ).before( '' );
	}

	if ( $('body').hasClass('searchResults') || $('body').hasClass('fullRecord') ) {
		// per WebX request, documented in SN TASK1351197
		// add an "Advanced Search" link next to the already present "Start Over" element
		//
		// The CSS element ID is 'searchWithinLinks' and if it's not present, this will gracefully be skipped.
		$advancedSearchLink = $('<a href="/search.jsp#tab2">Advanced Search</a>');
		$('#searchWithinLinks').html("");
		$('#searchWithinLinks').append("&nbsp;&nbsp;");
		$('#searchWithinLinks').append($advancedSearchLink);
	}



	generateMissingBookAnchorElement(
		$('tr.titleRow > td[align="left"] > span.recordTitle').text(),
		$('.bookAuthor:first').text(),
		'');

	if ($('A.item-location-info').size() == 0) {
		generateLocationGuideLinks('location');
	}

	if ($('A.item-location-info').size() > 0) {
		$('TR[data-locationcode]').each(function(index) {
			
			$('A.item-location-info', this).click(function(evt) {
				evt.stopImmediatePropagation();
				var callno = $(this).parents('[data-locationcode]').attr('data-callnumber');
				var collectioncode = $(this).parents('[data-locationcode]').attr('data-locationcode');
				var library = $(this).parents('[data-locationcode]').attr('data-sublibrary');
				var titlestring = $(this).parents('TR[institution="Duke"]').attr('data-titlestring');
				
				if (titlestring == "null") {
					titlestring = $('SPAN.recordTitle').text();
				}

				$.ajax({
					url					: '//library.duke.edu/locguide/mapinfo',
					beforeSend	: function() {
						$.fancybox.showLoading();
						return true;
					},
					data				: {
						collection_code	: collectioncode,
						callno					: callno,
						sublibrary			: library,
						titlestring			: titlestring,
						ajax						: 'yes'
					},
					type 				: 'POST',
					cache 			: false,
					success 		: function(data, txtstatus, o) {
						$.fancybox.hideLoading();
						var popupTitle = [];
						popupTitle.push('<h3>' + data.postdata.titlestring);
						if (data.postdata.callno.length > 0) {
							popupTitle.push(' // ' + data.postdata.callno);
						}
						popupTitle.push('</h3>');
						var titleElement = popupTitle.join('');
						if (data.theme == 'area-map') {
							$areamap = $(
									'<div><img src="//' + data.server_name + data.media_path + data.image + '"/>' +
									'<div><span><strong>Building</strong><br />' + data.building + '</span><br /></div>' + 
									'<div><span><strong>Area/Floor</strong><br />' + data.area + '</span></div></div>'
								);
							$.fancybox.open($areamap, {
								autoSize: false, 
								width: 580, 
								height: 'auto',
								afterLoad : function() {
									//titleElement = '<h3>' + data.postdata.titlestring + ' // ' + data.postdata.callno + '</h3>';
									this.inner.prepend(titleElement);
								}
							});
						} else if (data.theme == 'external-link') {
							$externallink = $(
									'<div>' +
									'<h2>' + data.extlink_title + '</h2>' +
									'<p>' + data.generic_msg_content + '</p>' +
									'<p><a href="' + data.external_url + '" target="_blank">' + data.extlink_text + '</a></p>'
								);
							$.fancybox.open($externallink, {
								autoSize: false, 
								width: 600, 
								height: 'auto',
								afterLoad : function() {
									//titleElement = '<h3>' + data.postdata.titlestring + ' // ' + data.postdata.callno + '</h3>';
									this.inner.prepend(titleElement);
								}
							});
						}
					},
					error 			: function(o, txtstatus, err) {
						$.fancybox.hideLoading();
						debugger;
					}
				});
				return evt.preventDefault();
			});
		});
	}


	generateBookplateLightboxLinks('Donor');
	generateLinkResolverLogoLinks('primaryurls', 'duke-db', true);

	initDBRecordTitleLinks();


	
	console.log('duke functions init ... complete!');
	//correctRadioButtonLabels();
});

// add Browse by Subject Tab that points to external Database Finder browse page http://library.duke.edu/databases/
$(document).ready(function(){
	$('.ui-tabs-nav', 'body[applicationskinname="duke-db"]').append('<li><a href="//library.duke.edu/databases/"><span>Browse by Subject</span></a></li>');
});
