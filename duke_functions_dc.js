// Additional Javascript to enhance the display of results and items in the 
// Duke Endeca interface when Duke Digital Collections are displayed.

$(document).ready(function(){	


	// For every link to view a digital collection, prepend the corresponding collection icon.
	$('.repositoryURL a').each(function(){
						
		var collectionUrl = $(this).attr('href');

		if(collectionUrl.match('http://library.duke.edu/digitalcollections/')){
			var collectionSlug = collectionUrl.replace('http://library.duke.edu/digitalcollections/','').replace('/','');
			var collectionSmallIcon = 'http://library.duke.edu/digitalcollections/media/app/images/icons/collection/'+collectionSlug+'_30x30.jpg';

			$(this).prepend('<img src="http://library.duke.edu/digitalcollections/media/app/images/icons/collection/'+collectionSlug+'_30x30.jpg" class="dcIcon16" alt="icon"/>');
		};




		// Confirm that an icon exists before trying to render it on the page. Commented out for now; more testing to do.
		/*
		var self = $(this);		
		$.ajax({
		    url: collectionSmallIcon,
		    type:'HEAD',
		    error: function()
		    {
		        console.log('Icon file does not exist. Do not try to render it.');
		    },
		    success: function()
		    {
				console.log('Icon file '+collectionSmallIcon+' exists.');
				$(self).prepend('<img src="http://library.duke.edu/digitalcollections/media/app/images/icons/collection/'+collectionSlug+'_30x30.jpg" class="dcIcon16" alt="icon"/>');
		    }
		});
		*/

	});

	// Simplify the text in the Online Access field	
	$('td.digitalCollectionURL a').text("View Item").prepend('<img src="http://library.duke.edu/imgs/common/icons/icon-duke-view-item.png" class="dcIcon16" alt="icon"/>');	
	
	// Make the thumbnail link directly to the item view
	$('td.digitalCollectionURL a').each(function(){
		var itemUrl = $(this).attr('href');
		
		// This depends heavily on the table nesting to associate the thumbnail with the item, since there are very few classes defined in the html elements.
		var thumbImageLink = $(this).parents('table').eq(2).find('img.bookCover').parent('a');
		
		if (thumbImageLink) {
			thumbImageLink.addClass('directLink');
			thumbImageLink.attr('href',itemUrl);
			thumbImageLink.append('<div class="thumbOverlay">VIEW ITEM &raquo;</div>');
		};
		
					
	});


	// If a Digital Collection is specified as a facet filter selection...		
	var dcFacet = $('div.facetSelection').filter(':contains("Digital Collection")');
	//console.log("dcFacet = "+dcFacet);

	if (dcFacet) {

			// Determine the first digital collection linked to in the results
			var firstCollectionUrl = $('td.repositoryURL:first a').attr('href');
			//console.log("firstCollectionUrl = " + firstCollectionUrl);

			// If a digital collection link is found in results, define variables

			if(firstCollectionUrl) {

				var firstCollectionSlug = firstCollectionUrl.replace('http://library.duke.edu/digitalcollections/','').replace('/','');
				//console.log("firstCollectionSlug = " + firstCollectionSlug);

				var firstCollectionName = dcFacet.children("span.breadcrumbFacetValue").text();

				var firstCollectionIcon = 'http://library.duke.edu/digitalcollections/media/app/images/icons/collection/'+firstCollectionSlug+'_60x60.jpg';
					//console.log("firstCollectionIcon = " + firstCollectionIcon);


				// Put in a styled box for digital collection context, link to portal and digital collection home...
				dcFacet.prepend('<div class="dcContextBox"><div class="dcColumnImg"><a href="'+firstCollectionUrl+'"><img src="'+firstCollectionIcon+'" alt="" class="dcIcon60" /></a></div><div class="dcColumnColl"><a href="'+firstCollectionUrl+'" class="dcColumnCollTitle">'+firstCollectionName+'</a><br/><a href="http://library.duke.edu/digitalcollections/" class="dcColumnDcHome">Digital Collections Home</a></div><div style="clear:both;"></div></div>');
			};

	};

});