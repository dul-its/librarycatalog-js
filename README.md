# Library Catalog Javascripts

## Mission Statement
The Javascript code, specifically "duke_functions_2.js" is referenced (or included) on the legacy Endeca 
discovery search results and full record pages.  

The script intends to do the following functions when a page has completed loading:
* Generate Location Guide links
* Provide a "Live" Item Status

## Library Catalog "Live Status Update"

The code logic that provides the "live" item status display for Endeca pages is contained in 
**duke_functions_2.js**.

**_NOTE_** - *The functions and/or logic in the above file are not limited to retrieve a 'Live Status' for a title.* 

The code "workflow" for the Live Status Update starts in the function named, `performLiveCircLookups()`. Below is a general outline of 
the logic flow.

### Step 1: Query an item's ALEPH status information

This is done with an AJAX call to https://library.duke.edu/librarycatalog/circstatus/*system_number*, where *system_number* is a 
9-character string known by ALEPH.  This call returns a JSON object, where each key represents the 1..N barcodes belonging to 
an item, and the value associated with that barcore is a hash/dictionary consisting of (but not limited to) these keys:

`due-date`  
`loan-status`  
`sub-library`  
`sub-library-code`  
`location` (i.e. call number)

### Step 2: Parse the JSON / Determine Which Key to Use
For each barcode dictionary, the `due-date` value is inspected and if it not an empty string, the logic checks to see 
if the string contains a date (typically in MM/DD/YYYY format).  If the `due-date` value contains a date, the logic proceeds using this field.

##### When `due-date` is not useful
If the `due-date` is in a format similar to this -- "In Process (O)", then the `loan-status` is used to attempt to determine an item's status.

### Step 3: Determine The Item Status
Once it has been determined which of `due-date` or `loan-status` to use, the actual item status is determined. This logic has been 
refactored numerous times based on both internal ITS use cases and use cases requested by WebX.
