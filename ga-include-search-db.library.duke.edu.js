// Google Analytics tracker insertion fragment (search.library.duke.edu)
if ((window.location.hostname == "search.library.duke.edu") || (window.location.hostname == "db.library.duke.edu")) {
	var gaAccount = 'UA-24395410-1';
	if (window.location.hostname == "db.library.duke.edu") {
		gaAccount = 'UA-24397945-1';
	}
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', gaAccount]);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
}
